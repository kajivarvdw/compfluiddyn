%%
% This script plots the variation of the temperature
% of a rod after one end being put in a fire

clf;
clc;
%data = load('../output/600op35000/output.dat');
%data = load('../output/600op35000/output.dat');
%data = load('../output/outputGlacier1D_EF.dat');
%initCond = load('../output/initCond.dat');


[maxT, sizeX] = size(data);
dx = 50200. / sizeX;
pause on;
x = (1:sizeX)*dx;   

grey=[0.8,0.8,0.8];
grey2=[0.5,0.5,0.5];

figure(2);
hold on;
plot(x, ones(sizeX) * 312.5, '-', 'Color', grey);
plot(x(130:270), data(500,130:270), 'Color', grey);
plot(x(130:270), data(1000,130:270), 'Color', grey);
plot(x(130:270), data(2200,130:270), 'Color', grey);
plot(x(130:270), data(4000,130:270), 'k');
plot(x, data(1,:), '--k');
xlim([10000 40000])

xlabel('x (m)');
ylabel('height (m)');

hold off;

%figure(1);
%previouslyStable = false;
%for i = 2:10:(maxT-1)
%    plot(x, data(i,:), x, data(1,:))
%    maxdiff = max(abs(data(1,:) - data(i,:)));
%    title(['Timestep: ', num2str(50*i), ', diff: ' num2str(maxdiff)])
%    if maxdiff < 0.004 && previouslyStable == false;
%        disp(['Stable at t = ' num2str(50*i)])
%        previouslyStable = true;
%    end
%    if maxdiff > 0.004 && previouslyStable == true
%        disp(['Again unstable at t = ' num2str(50*i)]);
%        previouslyStable = false;
%    end
%    drawnow
%end

